
-- a. po paralelka i predmet
CREATE OR REPLACE VIEW  HISTOGRAM_PO_PARALEKA_PREDMET  AS 
select SCHOOL_YEAR, SCHOOL, SCHOOL_CODE, SCHOOL_CLASS_ID, SUBJECT,

sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki


from ocenki_prosireno
group by SCHOOL_YEAR, SCHOOL, SCHOOL_CODE, SCHOOL_CLASS_ID, SUBJECT;

-- b. paralelka


-- c. uciliste i predmet
CREATE OR REPLACE FORCE VIEW  HISTOGRAM_PO_UCILISHTE_REDMET AS 
select SCHOOL, SCHOOL_CODE, SUBJECT,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki

from ocenki_prosireno
group by SCHOOL, SCHOOL_CODE, SUBJECT;


--d. po ucilista
CREATE OR REPLACE FORCE VIEW  HISTOGRAM_PO_UCILISHTA AS 
select school,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno
group by school;

-- e. po grad
create or replace view HISTOGRAM_PO_GRAD AS
select grad,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno
group by grad;


-- f. po nasoka
create or replace view HISTOGRAM_PO_NASOKA as
select SCHOOL_CLASS_NAME,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno
group by SCHOOL_CLASS_NAME;


-- g. po ucebna godina
create  or replace view HISTOGRAM_PO_GODINA as
select SCHOOL_YEAR,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno
group by SCHOOL_YEAR;

-- h. po predmet
create or replace view HISTOGRAM_PO_PREDMET as
select SUBJECT,
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno
group by SUBJECT;

-- i. vo drzava
CREATE OR REPLACE VIEW  HISTOGRAM_PO_DRZAVA AS 
select 
sum(SEMESTER_1_RESULT_1) as semester_1_edinici, sum(SEMESTER_1_RESULT_2) as semester_1_dvojki, sum(SEMESTER_1_RESULT_3) as  semester_1_trojki,
sum(SEMESTER_1_RESULT_4) as semester_1_chetvorki, sum(SEMESTER_1_RESULT_5) as semester_1_petki,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_edinici, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_dvojki, sum(SEMESTER_2_RESULT_3) as  SEMESTER_2_trojki,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_chetvorki, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_petki,

sum(FINAL_GRADE_RESULT_1) as final_edinici, sum(FINAL_GRADE_RESULT_2) as final_dvojki, sum(FINAL_GRADE_RESULT_3) as final_trojki,
sum(FINAL_GRADE_RESULT_4) as final_chetvorki, sum(FINAL_GRADE_RESULT_5) as final_petki
from ocenki_prosireno;
 