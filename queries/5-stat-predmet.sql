CREATE OR REPLACE VIEW  "PREDMET_PO_NASOKA_I_GODINA" AS 
select school_class_name,GRADE, count(distinct lower(SUBJECT)) as broj_na_PREDMETI
from ocenki
group by school_class_name,GRADE
order by broj_na_PREDMETI;

CREATE OR REPLACE FORCE VIEW  "PREDMETI_PO_UCILISHTA" AS 
SELECT school, school_name, count(distinct subject) 
from ocenki_prosireno
group by school, school_name;