-- Во овој поглед табелата оценки ја прошируваме со уште 3 колони
-- Сите аналитики да се прават врз овој поглед, а не директно врз OCENKI, да се избегне копирање на регуларниве изрази

drop view OCENKI_PROSIRENO;
--create or replace view OCENKI_PROSIRENO as
create materialized view OCENKI_PROSIRENO as
select ocenki.*,
replace(regexp_replace(school, '(,,|„|“|'''')', '"'), 'на ГС', '') as school_name,
upper(trim(trim(',' from regexp_substr(replace(regexp_replace(school, '(,,|„|“|'''')', '"'), 'на ГС', ''), '[^"-]+$')))) as grad,
case when regexp_like(school, '^(оу|пу|поу|п.о.у|ппоу|пооу|подрачно|цоу)\W', 'i') then 'основно' else 'средно' end as tip
from ocenki;

create bitmap index OCENKI_PROSIRENO_SCHOOL on ocenki_prosireno(school);
create bitmap index OCENKI_PROSIRENO_tip on ocenki_prosireno(tip);
create bitmap index OCENKI_PROSIRENO_grad on ocenki_prosireno(grad);
create bitmap index OCENKI_PROSIRENO_sch_year on ocenki_prosireno(school_year);
create bitmap index OCENKI_PROSIRENO_subject on ocenki_prosireno(subject);
create bitmap index OCENKI_PROSIRENO_grade on ocenki_prosireno(grade);

-- Објаснување на прашалникот. Од името на училиштето:
-- 1. ги заменуваме две запирки ,,  кирилични наводници „“ и два апострофа '' со латинични " (regexp_replace)
-- 2. потоа заменуваме еден непотребен стринг 'на ГС' (што значи на Град Скопје) (replace)
-- 3. и потоа *името на градот* го читаме така што читаме од крајот на стрингот читаме до првиот наводник или цртичка. (regex_substr)
-- 4. со трим чистиме празни места на почеток и крај и (trim)
-- 5. сето го правиме со големи букви (upper) за да се разрешат случаи ист град со различни стригови што се разликуваат во мали и големи букви.