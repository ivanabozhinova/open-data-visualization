

-- a. Po paralelka i predmet

CREATE OR REPLACE VIEW  "medijana_za_paralelka_predmet" AS 
select SCHOOL_YEAR, SCHOOL, SCHOOL_CODE, SCHOOL_CLASS_ID, SUBJECT,
ocenki_stat.medijana(SEMESTER_1_RESULT_1, SEMESTER_1_RESULT_2, SEMESTER_1_RESULT_3, SEMESTER_1_RESULT_4, SEMESTER_1_RESULT_5) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(SEMESTER_2_RESULT_1, SEMESTER_2_RESULT_2, SEMESTER_2_RESULT_3, SEMESTER_2_RESULT_4, SEMESTER_2_RESULT_5) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(FINAL_GRADE_RESULT_1, FINAL_GRADE_RESULT_2, FINAL_GRADE_RESULT_3, FINAL_GRADE_RESULT_4, FINAL_GRADE_RESULT_5) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno;


-- b. po paralelka
CREATE OR REPLACE VIEW  "medijana_za_paralelka" AS 
select school_class_id, ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno
group by school_class_id
order by school_class_id;


-- c. po predmet i uciliste
CREATE OR REPLACE VIEW  "medijana_za_premet_uciliste" AS 
select SCHOOL, SCHOOL_CODE, SUBJECT,
ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno
group by SCHOOL, SCHOOL_CODE, SUBJECT
order by school_name;

-- d. po uciliste
CREATE OR REPLACE VIEW  "medijana_za_uciliste" AS 
select SCHOOL, SCHOOL_CODE,
ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno
group by SCHOOL, SCHOOL_CODE
order by school_name;

-- e. po grad



-- f. po nasoka 
CREATE OR REPLACE VIEW  "medijana_za_nasoka" ("SCHOOL_CLASS_NAME", "SEMESTAR_1_MEDIJANA", "SEMESTAR_2_MEDIJANA", "FINAL_GRADE_MEDIJANA") AS 
select school_class_name,
ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno
group by school_class_name
order by school_class_name;
 
-- g. po ucebna godina
CREATE OR REPLACE VIEW  "medijana_za_ucebna_godina" ("SCHOOL_YEAR", "SEMESTAR_1_MEDIJANA", "SEMESTAR_2_MEDIJANA", "FINAL_GRADE_MEDIJANA") AS 
select school_year,
ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno
group by school_year
order by school_year;

--h. po predmet

--i. po drzava
CREATE OR REPLACE VIEW  "medijana_za_drzava" ("SEMESTAR_1_MEDIJANA", "SEMESTAR_2_MEDIJANA", "FINAL_GRADE_MEDIJANA") AS 
select ocenki_stat.medijana(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MEDIJANA,
ocenki_stat.medijana(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MEDIJANA,
ocenki_stat.medijana(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MEDIJANA
from ocenki_prosireno;
 
