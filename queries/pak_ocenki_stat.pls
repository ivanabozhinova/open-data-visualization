create or replace package OCENKI_STAT as
  function moda(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number;
  function medijana(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number;
  function quantile(edinici number, dvojki number, trojki number, cetvorki number, petki number, quantile number) return number;
  function outlier_lower_fence(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number;
  function outlier_upper_fence(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number;
end OCENKI_STAT;