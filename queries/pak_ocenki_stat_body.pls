create or replace package body OCENKI_STAT as
  
  type niza_ocenki is varray(5) of number;

  function moda(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number is
    ret number;
    maxx number;
  begin
    ret := 1;
    maxx := edinici;
    if (dvojki > maxx) then
      ret := 2;
      maxx := dvojki;
    end if;
    if (trojki > maxx) then
      ret := 3;
      maxx := trojki;
    end if;
    if (cetvorki > maxx) then
      ret := 4;
      maxx := cetvorki;
    end if;
    if (petki > maxx) then
      ret := 5;
      maxx := petki;
    end if;
    if ret = 1 and maxx = 0 then
      return null; -- ili return 0;
    end if;
    return ret;
  end moda;

  function medijana(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number is
    niza niza_ocenki;
    suma number;
    i pls_integer := 1;
    cnt pls_integer := 0;
    median_idx pls_integer;
  begin
    niza := niza_ocenki(edinici, dvojki, trojki, cetvorki, petki);
    suma := edinici + dvojki + trojki + cetvorki + petki;
    if suma = 0 then
      return null; -- ili return 0
    end if;
    if (suma mod 2 = 1) then
      median_idx := (suma+1)/2;
    else
      median_idx := (suma)/2;
    end if;
    while i <= 5 loop
      cnt := cnt + niza(i);
      if (cnt >= median_idx) then
        exit;
      end if;
      i := i + 1;
    end loop;
    if (suma mod 2 = 0 and median_idx = cnt) then
      return i+0.5;
    end if;
    return i;
  end medijana;
  
  function quantile(edinici number, dvojki number, trojki number,
    cetvorki number, petki number, quantile number) return number is
    --formula http://www.haiweb.org/medicineprices/manual/quartiles_iTSS.pdf
    cnt number := 0;
    i number := 0.5;
    niza niza_ocenki;
    suma number;
    idx number;
    idx_decimalen_del number;
    val_1 number;
    val_2 number;
  begin
    niza := niza_ocenki(edinici, dvojki, trojki, cetvorki, petki);
    suma := edinici + dvojki + trojki + cetvorki + petki;
    if suma = 0 then
      return null; -- ili return 0
    end if;
    idx := floor(quantile * suma);
    idx_decimalen_del := quantile * suma - idx;
    while i <= 5 loop
      cnt := cnt + niza(i);
      if (cnt >= idx) then
        val_1 := i;
        exit;
      end if;
      i := i + 1;
    end loop;
    cnt := cnt - niza(i);
    while i <= 5 loop
      cnt := cnt + niza(i);
      if (cnt >= idx + 1) then
        val_2 := i;
        exit;
      end if;
      i := i + 1;
    end loop;
    if (idx_decimalen_del = 0) then
      return (val_1 + val_2)/2;
      --return idx + 0.5;
    else
      return val_2;
      --return idx + 1;
    end if;
  end quantile;
  
  function outlier_lower_fence(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number is
    q1 number;
    q3 number;
    iqr number;
  begin
    q1 := quantile(edinici, dvojki, trojki, cetvorki, petki, 0.25);
    q3 := quantile(edinici, dvojki, trojki, cetvorki, petki, 0.75);
    iqr := q3-q1;
    return q1 - 1.5*iqr;
  end;
  function outlier_upper_fence(edinici number, dvojki number, trojki number, cetvorki number, petki number) return number is
    q1 number;
    q3 number;
    iqr number;
  begin
    q1 := quantile(edinici, dvojki, trojki, cetvorki, petki, 0.25);
    q3 := quantile(edinici, dvojki, trojki, cetvorki, petki, 0.75);
    iqr := q3-q1;
    return q3 + 1.5*iqr;
  end;
end OCENKI_STAT;