--------------------------------------------------------
--  File created - вторник-јуни-09-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence OCENKI_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "OCENKI_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table UCILISTA_LOKACII
--------------------------------------------------------

  CREATE TABLE "UCILISTA_LOKACII" 
   (	"SCHOOL_CODE" NUMBER(30,0), 
	"SCHOOL" VARCHAR2(255 BYTE), 
	"SCHOOL_NAME" VARCHAR2(255 BYTE), 
	"GRAD" VARCHAR2(255 BYTE), 
	"TIP" VARCHAR2(20 BYTE), 
	"LATITUDE" NUMBER(30,25), 
	"LONGITUDE" NUMBER(30,25),
	constraint UCLISTA_LOKACII_PK primary key ("SCHOOL")
   );
--------------------------------------------------------
--  DDL for Table GRADOVI_LOKACII
--------------------------------------------------------

  CREATE TABLE "GRADOVI_LOKACII" 
   (	"GRAD" VARCHAR2(255 BYTE), 
	"LATITUDE" NUMBER, 
	"LONGITUTDE" NUMBER,
	constraint GRADOVI_LOKACII_PK primary key("GRAD")
   );
--------------------------------------------------------
--  DDL for Table OCENKI
--------------------------------------------------------

  CREATE TABLE "OCENKI" 
   (	"ID" NUMBER, 
	"SCHOOL_YEAR" VARCHAR2(255 BYTE), 
	"SCHOOL_CODE" NUMBER, 
	"SCHOOL" VARCHAR2(255 BYTE), 
	"SUBJECT" VARCHAR2(255 BYTE), 
	"GRADE" VARCHAR2(60 BYTE), 
	"SCHOOL_CLASS_ID" NUMBER, 
	"SCHOOL_CLASS_TYPE" VARCHAR2(255 BYTE), 
	"SCHOOL_PROGRAM_TYPE" VARCHAR2(255 BYTE), 
	"SCHOOL_CLASS_CODE" VARCHAR2(255 BYTE), 
	"SCHOOL_CLASS_NAME" VARCHAR2(255 BYTE), 
	"SEMESTER_1_RESULT_1" NUMBER, 
	"SEMESTER_1_RESULT_2" NUMBER, 
	"SEMESTER_1_RESULT_3" NUMBER, 
	"SEMESTER_1_RESULT_4" NUMBER, 
	"SEMESTER_1_RESULT_5" NUMBER, 
	"SEMESTER_2_RESULT_1" NUMBER, 
	"SEMESTER_2_RESULT_2" NUMBER, 
	"SEMESTER_2_RESULT_3" NUMBER, 
	"SEMESTER_2_RESULT_4" NUMBER, 
	"SEMESTER_2_RESULT_5" NUMBER, 
	"FINAL_GRADE_RESULT_1" NUMBER, 
	"FINAL_GRADE_RESULT_2" NUMBER, 
	"FINAL_GRADE_RESULT_3" NUMBER, 
	"FINAL_GRADE_RESULT_4" NUMBER, 
	"FINAL_GRADE_RESULT_5" NUMBER,
	constraint OCENKI_PK primary key("ID")
   );


  CREATE OR REPLACE TRIGGER "bi_OCENKI" 
  before insert on "OCENKI"              
  for each row 
begin  
  if :new."ID" is null then
    select "OCENKI_SEQ".nextval into :new."ID" from sys.dual;
  end if;
end;

/
ALTER TRIGGER "bi_OCENKI" ENABLE;
