
create or replace view "Broj_na_ucilista" as
select count(distinct SCHOOL_CODE) as Broj_na_ucilista
from OCENKI;

CREATE OR REPLACE VIEW  "Broj_ucilista_po_grad" AS 
select count(distinct school) as broj_ucilista_po_grad, grad
from ocenki_prosireno
group by grad;


create or replace view Broj_na_ucilista_po_tip as
select count(distinct school) as broj_na_ucilista, school_program_type
from ocenki
group by school_program_type;


create or replace view "Broj_na_ucilista_po_grad_i_tip" AS 
select grad, tip, count(distinct lower(school)) as broj_na_ucilista
from ocenki_prosireno
group by grad, tip order by grad;


create or replace view "Broj_na_strani_po_klaas" AS 
select count(distinct lower(school)) as broj_na_ucilista, grad, school_class_name
from ocenki_prosireno
group by grad, school_class_name;


create or replace view UCILISTA_BEZ_GRAD as
-- треба да се разрешат рачно случаи каде нема наводници или цртичка
-- тие се дадени со следниов селект
select distinct school_code, school
from ocenki
where regexp_like(school, '^[^,"„“''-]+$')
union
-- или каде името на населеното место е во наводниците или не е наведено
select distinct school_code, school
from OCENKI_PROSIRENO
where grad is null;

update ocenki
set school='Државно музичко училиште - Битола'
where school='Државно музичко училиште Битола';

update ocenki
set school='ПУ Крсте П. Мисирков - Кишава'
where school='ПУ Крсте П. Мисирков Кишава';

update ocenki
set school='ПУ Крсте П. Мисирков - Кравари'
where school='ПУ Крсте П. Мисирков Кравари';

update ocenki
set school='ОУ Даме Груев - Битола ПУ с.Долно Оризари'
where school='ОУ Даме Груев Битола ПУ с.Долно Оризари';

update ocenki
set school='ОУ Даме Груев - Битола ПУ с.Карамани'
where school='ОУ Даме Груев Битола ПУ с.Карамани';

update ocenki
set school='СОЗУ,,Кузман Шапкарев,, Битола'
where school='СОЗУ,,Кузман Шапкарев,,';

select *
from ocenki
where lower(school) like '%владо кантарџиев%';

update ocenki
set school='ПУ Богородица - Богородица' -- матично основно Владо Кантарџиев
where school='ПУ Богородица';

update ocenki
set school='ПОУ Ванчо Прке - с. Габрово'
where school='ПОУ Ванчо Прке с. Габрово';

update ocenki
set school='ПОУ Ванчо Прке - с. Звегор'
where school='ПОУ Ванчо Прке с. Звегор';

update ocenki
set school='П.О.У. „Маршал Тито“ - Банско'
where school='П.О.У. „Маршал Тито - Банско“';

update ocenki
set school='П.О.У. „Маршал Тито“ - Габрово'
where school='П.О.У. „Маршал Тито - Габрово“';


create or replace view UCILISTA_PROSIRENO as
select distinct school_code, school, school_name, grad, tip
from ocenki_prosireno
order by grad, school_code;

update ocenki
set school = 'ОУ"Јосип Броз Тито" - ПИРАВА, ВАЛАНДОВО'
where school = 'Јосип Броз Тито - ПИРАВА';
update ocenki
set school = 'ОУ"Јосип Броз Тито" - ЧАЛАКЛИ, ВАЛАНДОВО'
where school = 'Јосип Броз Тито - ЧАЛАКЛИ';

update ocenki
set school = 'ПОУ"ИДНИНА" ЧАИР'
where school = 'ПОУ"ИДНИНА ЧАИР';