

-- a. Po paralelka i predmet

CREATE OR REPLACE FORCE VIEW  "AVG_VAR_PARALELKA_PREDMET" ("SCHOOL_YEAR", "SCHOOL", "SCHOOL_CODE", "SCHOOL_CLASS_ID", "SUBJECT", "AVG_SEMESTER_1", "VAR_SEMESTER_1", "AVG_SEMESTER_2", "VAR_SEMESTER_2", "AVG_FINAL", "VAR_FINAL") AS 
  select SCHOOL_YEAR, SCHOOL, SCHOOL_CODE, SCHOOL_CLASS_ID, SUBJECT, 
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
from (
    select ocenki_so_sumi.*,
    decode(suma_semester_1, 0, 0,
        (SEMESTER_1_RESULT_1 * 1 + SEMESTER_1_RESULT_2 * 2 + SEMESTER_1_RESULT_3 * 3 +
        SEMESTER_1_RESULT_4 * 4 + SEMESTER_1_RESULT_5 * 5)/suma_semester_1)
        as avg_semester_1,
        decode(suma_semester_2, 0, 0,
        (SEMESTER_2_RESULT_1 * 1 + SEMESTER_2_RESULT_2 * 2 + SEMESTER_2_RESULT_3 * 3 +
        SEMESTER_2_RESULT_4 * 4 + SEMESTER_2_RESULT_5 * 5)/suma_SEMESTER_2)
        as avg_semester_2,
         decode(suma_final, 0, 0,
        (FINAL_GRADE_RESULT_1 * 1 + FINAL_GRADE_RESULT_2 * 2 + FINAL_GRADE_RESULT_3 * 3 +
        FINAL_GRADE_RESULT_4 * 4 + FINAL_GRADE_RESULT_5 * 5)/suma_final)
        as avg_final
    from (
            select ocenki_prosireno.*,
            (SEMESTER_1_RESULT_1 + SEMESTER_1_RESULT_2 + SEMESTER_1_RESULT_3+ SEMESTER_1_RESULT_4 + SEMESTER_1_RESULT_5 ) as suma_semester_1,
            (SEMESTER_2_RESULT_1 + SEMESTER_2_RESULT_2 + SEMESTER_2_RESULT_3+ SEMESTER_2_RESULT_4 + SEMESTER_2_RESULT_5) as suma_semester_2,
            (FINAL_GRADE_RESULT_1  + FINAL_GRADE_RESULT_2 + FINAL_GRADE_RESULT_3 + FINAL_GRADE_RESULT_4 + FINAL_GRADE_RESULT_5) as suma_final
            from ocenki_prosireno
    ) ocenki_so_sumi
)



-- b. po paralelka

CREATE OR REPLACE FORCE VIEW  "AVG_VAR_PARALELKA" ("SCHOOL_CLASS_ID", "AVG_SEMESTER_1", "VAR_SEMESTER_1", "AVG_SEMESTER_2", "VAR_SEMESTER_2", "AVG_FINAL", "VAR_FINAL") AS 
  select
SCHOOL_CLASS_ID,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
from 
(select SCHOOL_CLASS_ID,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,

sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from 
ocenki_avg_sum
group by SCHOOL_CLASS_ID 
) ocenki_avg_sum_class

order by avg_final




-- c. po predmet i uciliste

CREATE OR REPLACE FORCE VIEW  "AVG_VAR_PREDMET_UCILISTE" ("SCHOOL", "SCHOOL_CODE", "SUBJECT", "AVG_SEMESTER_1", "VAR_SEMESTER_1", "AVG_SEMESTER_2", "VAR_SEMESTER_2", "AVG_FINAL", "VAR_FINAL") AS 
  select SCHOOL, SCHOOL_CODE, SUBJECT,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select SCHOOL, SCHOOL_CODE, SUBJECT,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by SCHOOL, SCHOOL_CODE, SUBJECT
)

order by SCHOOL;




-- d. po uciliste

CREATE OR REPLACE FORCE VIEW  "AVG_VAR_UCILISTE" ("SCHOOL", "SCHOOL_CODE", "AVG_SEMESTER_1", "VAR_SEMESTER_1", "AVG_SEMESTER_2", "VAR_SEMESTER_2", "AVG_FINAL", "VAR_FINAL") AS 
  select SCHOOL, SCHOOL_CODE,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select SCHOOL, SCHOOL_CODE,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by SCHOOL, SCHOOL_CODE
)

order by SCHOOL;
 ​

 
 
 
-- e. po grad

CREATE OR REPLACE FORCE VIEW  "AVG_VAR_GRAD" ("SCHOOL", "SCHOOL_CODE", "AVG_SEMESTER_1", "VAR_SEMESTER_1", "AVG_SEMESTER_2", "VAR_SEMESTER_2", "AVG_FINAL", "VAR_FINAL") AS 
  select GRAD,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select GRAD,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by GRAD
)





-- f. po nasoka

create or replace view "AVG_VAR_NASOKA" as
  select SCHOOL_CLASS_NAME,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select SCHOOL_CLASS_NAME,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by SCHOOL_CLASS_NAME
)

 ​
 
 
 -- g. po ucebna godina
 
 create or replace view "AVG_VAR_UCEBNA_GODINA" as
  select SCHOOL_YEAR,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select SCHOOL_YEAR,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by SCHOOL_YEAR
)



-- h. po predmet

create or replace view "AVG_VAR_PREDMET" as
  select SUBJECT,
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select SUBJECT,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

group by SUBJECT
)


-- i. po drzava

create or replace view "AVG_VAR_DRZAVA" as
  select 
avg_semester_1,
decode(suma_semester_1, 0, null, 1, 0,
    (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+
    SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+
    SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) )
as var_semester_1,
avg_semester_2,
decode(suma_semester_2, 0, null, 1, 0,
    (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+
    SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) )
as var_semester_2,
avg_final,
decode(suma_final, 0, null, 1, 0,
    (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+
    FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+
    FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) )
as var_final
    
from

(select 
avg(decode(ocenki_avg_sum.AVG_SEMESTER_1, 0, null, ocenki_avg_sum.AVG_SEMESTER_1)) as AVG_SEMESTER_1,
avg(decode(ocenki_avg_sum.AVG_SEMESTER_2, 0, null, ocenki_avg_sum.AVG_SEMESTER_2)) as AVG_SEMESTER_2,
avg(decode(ocenki_avg_sum.AVG_FINAL, 0, null, ocenki_avg_sum.AVG_FINAL)) as AVG_FINAL,
sum(SUMA_SEMESTER_1) as SUMA_SEMESTER_1,
sum(SUMA_SEMESTER_2) as SUMA_SEMESTER_2,
sum(SUMA_FINAL) as SUMA_FINAL,
sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1,
sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2,
sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3,
sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4,
sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5,

sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1,
sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2,
sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3,
sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4,
sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5,

sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1,
sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2,
sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3,
sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4,
sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5
from OCENKI_AVG_SUM

)