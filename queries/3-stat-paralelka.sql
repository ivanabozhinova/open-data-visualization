CREATE OR REPLACE VIEW  "Broj_na_paralelki_po_uciliste" AS 
select SCHOOL_CODE, SCHOOL, count(distinct SCHOOL_CLASS_CODE) as broj_paralelki
from OCENKI
group by SCHOOL_CODE, SCHOOL;
