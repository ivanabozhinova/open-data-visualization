
--a.
create or replace view "Broj_na_naseleni_mesta" as
select count(distinct GRAD) as Broj_na_naseleni_mesta
from ocenki_prosireno;

--b.
CREATE OR REPLACE VIEW "Nasoka_i_grad" AS 
select distinct lower(school_class_name) as nasoka, grad
from ocenki_prosireno
where school_class_name not like 'I%' and school_class_name not like 'V%' and
school_class_name not like 'прво%' and school_class_name not like 'второ%' and
school_class_name not like 'трето%' and school_class_name not like 'четврто%' and
school_class_name not like 'петто%' and school_class_name not like 'шесто%' and
school_class_name not like 'седмо%'  and school_class_name not like 'осмо%'
order by grad desc;

CREATE OR REPLACE VIEW GRADOVI AS
select distinct grad
from ocenki_prosireno;
