

-- a. Po paralelka i predmet

-- b. po paralelka
CREATE OR REPLACE FORCE VIEW  "moda_za_paralelki" ("SCHOOL_CLASS_ID", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select school_class_id, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by school_class_id
order by school_class_id;

-- c. po predmet i uciliste
CREATE OR REPLACE FORCE VIEW  "moda_za_predmet_i_uciliste" ("SUBJECT", "SCHOOL_NAME", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select subject, school_name, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by subject,school_name
order by subject;

-- d. po uciliste
CREATE OR REPLACE FORCE VIEW  "moda_za_uciliste" ("SCHOOL_NAME", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select school_name, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by school_name
order by school_name;


-- e. po grad

-- f. po nasoka 
CREATE OR REPLACE FORCE VIEW  "moda_za_nasoki" ("SCHOOL_CLASS_NAME", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select school_class_name, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by school_class_name;

-- g. po ucebna godina
CREATE OR REPLACE FORCE VIEW  "moda_za_ucebna_godina" ("SCHOOL_YEAR", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select school_year, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by school_year
order by school_year;
 

--h. po predmet 
CREATE OR REPLACE FORCE VIEW  "moda_za_predmet" ("SUBJECT", "SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select subject, ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno
group by subject
order by subject;
 

--i. po drzava
CREATE OR REPLACE FORCE VIEW  "moda_za_drzavi" ("SEMESTAR_1_MODA", "SEMESTAR_2_MODA", "FINAL_GRADE_MODA") AS 
  select ocenki_stat.moda(sum(SEMESTER_1_RESULT_1), sum(SEMESTER_1_RESULT_2), sum(SEMESTER_1_RESULT_3), sum(SEMESTER_1_RESULT_4), sum(SEMESTER_1_RESULT_5)) as SEMESTAR_1_MODA,
ocenki_stat.moda(sum(SEMESTER_2_RESULT_1), sum(SEMESTER_2_RESULT_2), sum(SEMESTER_2_RESULT_3), sum(SEMESTER_2_RESULT_4), sum(SEMESTER_2_RESULT_5)) as SEMESTAR_2_MODA,
ocenki_stat.moda(sum(FINAL_GRADE_RESULT_1), sum(FINAL_GRADE_RESULT_2), sum(FINAL_GRADE_RESULT_3), sum(FINAL_GRADE_RESULT_4), sum(FINAL_GRADE_RESULT_5)) as FINAL_GRADE_MODA from ocenki_prosireno;
 