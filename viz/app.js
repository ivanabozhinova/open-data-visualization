process.env['NLS_LANG'] = '.UTF8'; //za oracle

var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var oracledb = require('oracledb');

var config = require('./config.json');

var routes = require('./routes/index');

var app = express();

app.set('db', null);

oracledb.createPool(
{
    user          : config.database.user,
    password      : config.database.password,
    connectString : config.database.connectString,
    poolMax: 20
},
function(error, pool) {
    if (error) {
        console.error(error.message);
        return;
    }

    console.log('pool!', pool);

    app.set('db', pool);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components')));

app.use('/', routes);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

function exitHandler(options, err) {
	if (app.get('db') != null) {
        app.get('db').terminate(function(error) {
			if (error) {
				console.error(error.message);
			} else {
				console.log("All DB connections closed successfuly.")
			}
			process.exit(0);
        });
    } else {
		process.exit(0);
    }
}

//process.on('exit', exitHandler);
//process.on('SIGINT', exitHandler);


module.exports = app;
