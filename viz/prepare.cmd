@echo off

if NOT "%VS100COMNTOOLS%"=="" set VISUAL_STUDIO=%VS100COMNTOOLS%
if NOT "%VS110COMNTOOLS%"=="" set VISUAL_STUDIO=%VS110COMNTOOLS%
if NOT "%VS120COMNTOOLS%"=="" set VISUAL_STUDIO=%VS120COMNTOOLS%
if "%VISUAL_STUDIO%"=="" (
	echo visual studio not installed
	goto error
)

set PYTHON=C:\Python27\python.exe
if "%PYTHON:~-4%" == ".exe" if EXIST "%PYTHON%" goto py_ok
if [%1] NEQ [] set PYTHON=%1
if "%PYTHON:~-4%" == ".exe" if EXIST "%PYTHON%" goto py_ok
if [%2] NEQ [] set PYTHON=%2
if "%PYTHON:~-4%" == ".exe" if EXIST "%PYTHON%" goto py_ok else (
	echo Python not found, exiting.
	goto error
)
:py_ok

set ORACLE_IC=C:\oracle\instantclient
if EXIST "%ORACLE_IC%\oci.dll" goto oci_ok
if [%1] NEQ [] set ORACLE_IC=%1
if EXIST "%ORACLE_IC%\oci.dll" goto oci_ok
if [%2] NEQ [] set ORACLE_IC=%2
if EXIST "%ORACLE_IC%\oci.dll" goto oci_ok else (
	echo Oracle instant client not found, exiting.
	goto error
)
:oci_ok
set OCI_LIB_DIR=%ORACLE_IC%\sdk\lib\msvc
set OCI_INC_DIR=%ORACLE_IC%\sdk\include
call "%VISUAL_STUDIO%\..\..\VC\vcvarsall.bat" x64
set path=%PYTHON%;%ORACLE_IC%;%path%

call npm install oracledb
call npm update
goto exit

:error
echo .
echo skriptata avtomatski kje se obide da gi najde potrebnite alatki za kompajliranje ako se instalirani na slednive mesta:
echo Python2.7		c:\Python27
echo Oracle instant client	c:\oracle\instantclient
echo .
echo inaku, alatkata se koristi vaka:
echo %~n0 python2.7_exe_path instant_client_folder
goto exit

:exit