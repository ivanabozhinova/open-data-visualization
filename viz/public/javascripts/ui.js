﻿window.ODV = window.ODV || {};

window.ODV.Router = new (Backbone.Router.extend({
  routes: {
    'godina/:godina/obrazovanie/:obrazovanie/studiska-godina/:studiska_godina/predmet/:predmet/:tip_ime/:tip': 'mod',
  },

  mod: function() {
    var args = _.toArray(arguments);

    this.trigger('odv:godina', args[0]);
    this.trigger('odv:obrazovanie', args[1]);
    this.trigger('odv:studiska-godina', args[2]);
    this.trigger('odv:predmet', args[3]);
    this.trigger('odv:' + args[4].toLowerCase(), args[5]);
    this.trigger('odv', this.current());
  },

  current: function() {
    var history = Backbone.history.getFragment().split('/');

    return _.object(
      _.filter(history, function(n, i) { return i % 2 == 0; }),
      _.filter(history, function(n, i) { return i % 2 == 1; }));
  },

  navigate: function() {
    if (_.isObject(arguments[0])) {
      var current = this.current();

      if (arguments[0]['grad'] || arguments[0]['uciliste']) {
        delete current['grad'];
        delete current['uciliste'];
      }

      arguments[0] = _.merge({}, current, arguments[0]);

      var path = [ 'godina',
        arguments[0]['godina'],
        'obrazovanie',
        arguments[0]['obrazovanie'],
        'studiska-godina',
        arguments[0]['studiska-godina'],
        'predmet',
        arguments[0]['predmet'] ];

      if (arguments[0]['grad']) {
        path.push('grad');
        path.push(arguments[0]['grad']);
      } else {
        if (!arguments[0]['uciliste']) {
          throw "Nema uciliste nitu grad!"
        }

        path.push('uciliste');
        path.push(arguments[0]['uciliste']);
      }

      arguments[0] = path.join('/');
    }

    Backbone.Router.prototype.navigate.apply(this, arguments);
  }
}));

window.ODV.Picker = Backbone.View.extend({
  initialize: function(props) {
    if (_.isFunction(props.transform)) {
      this.transform = props.transform;
    }

    if (_.isObject(props.collection)) {
      this.collection = props.collection;
    }

    var id = this.id = this.$el.attr('id');
    this.$data = this.$el.find('.data');

    this.$el.on('click .' + this.id, function(event) {
      var route = {};
      route[id] = $(event.target).attr('data-' + id);

      ODV.Router.navigate(route, { trigger: true });
    });

    ODV.Router.on('odv:' + this.id, this.render, this);

    if (this.collection) {
      this.fill(this.collection);
      this.collection.on('odv:loaded', function() {
        this.fill(this.collection);
      }, this);
    }
  },

  transform: function(x) {
    return x;
  },

  render: function(what) {
    this.$data.html(this.transform(what));

    return this;
  },

  fill: function(collection) {
    var data = this.$el.find('.dropdown-menu');

    data.empty();

    data.append("<li><a class='" + this.id + "' data-" + this.id + "='Сите'>Сите</a></li><li class='divider'></li>");

    collection.forEach(function(m) {
      data.append("<li><a class='" + this.id + "' data-" + this.id + "='" + m.get('name').replace(/\//g, '-') + "'>" + m.get('name') + "</a></li>");
    }, this);
  }
});


function renderHistogram(element, data) {
	var children = element.find('.histogram').children();
    
    //makismalna vrednost na histogramot (zaokruzena na 10%)
    var max = Math.max.apply(null,data);
    max = Math.ceil(max*10)*10

    //iscrtuvanje na histogramot
    _.forEach(children, function(c, i) {
      $(c).css({ 'width': data[i]/max*10000 + '%'});
      $(c).html((i+1) + "&nbsp(" + data[i] + ")");
    }, this);
    
    element.find('.range').html(max + " %&nbsp");
}


function renderStats(element, data) {
	element.find('.statistics').html(
               "<b>μ</b>  Средна Оценка: " + data[0] + "<br>" +
               "<b>σ<sup>2</sup></b>  Варијанса: " + data[1] + "<br>" +
               "<span style='text-decoration: overline;'><b>X</b></span>  Медијана: " + data[2] + "<br>" +
               "<b>Mo</b>  Мода: " + data[3] + "<br>");
}

$(function() {
  new ODV.Picker({ el: $('#obrazovanie'), collection: ODV.Tipovi });
  new ODV.Picker({ el: $('#godina'), collection: ODV.Godini });
  new ODV.Picker({ el: $('#studiska-godina'), collection: ODV.StudiskiGodini });
  new ODV.Picker({ el: $('#predmet'), collection: ODV.Predmeti });



  var request_id = -1;
  var lastParams = "";

  ODV.Router.on('odv', function(query) {
    request_id += 1;

    var current_request_id = request_id;

    console.log('Request: ', current_request_id);

    var params = {};

    params.tip = decodeURIComponent(query['obrazovanie']);
    params.school = decodeURIComponent(query['uciliste']);
    params.grad = decodeURIComponent(query['grad']);
    params.school_year = decodeURIComponent(query['godina'].replace(/-/g, '/'));
    params.grade = decodeURIComponent(query['studiska-godina']);
    params.subject = decodeURIComponent(query['predmet']);

    params = _.omit(params, function(value) {
      return _.isString(value) && (value.toLowerCase() == 'сите' || value == 'undefined');
    });
	
	var paramsStr = JSON.stringify(params);
	if (paramsStr === lastParams) {
		return;
	}
	
	lastParams = paramsStr;

    $.ajax({
      method: 'POST',
      url: '/ocenki',
      data: params,
      success: function(data, status, xhr) {
        console.log('Done: ', current_request_id);

        var data_object = {};

        _.forEach(data['metaData'], function(meta, i) {
          data_object[meta['name']] = data['rows'][0][i];
        });
        data_object.request = data['request'];

        ODV.Rezultati.clear({ silent: true });
        ODV.Rezultati.set(data_object);

        ODV.Rezultati.trigger('odv:loaded');
      },
      error: function(xhr, status, err) {
        ODV.Rezultati.trigger('odv:error');

        console.log(xhr, status, err);
      }
    });
  });

  ODV.Rezultati.on('change', function() {
    console.log('changed!', ODV.Rezultati);

    res = ODV.Rezultati.toJSON();
	if (res.request && res.request.body) {
		$('.panel pre.data').html(JSON.stringify(res.request.body, null, 4));
	} else {
		$('.panel pre.data').html("");
	}

    renderHistogram($('#panel1'), [
					Math.round(res["SEMESTER_1_RESULT_1"]/res["SUMA_SEMESTER_1"]*1000)/1000, 
					Math.round(res["SEMESTER_1_RESULT_2"]/res["SUMA_SEMESTER_1"]*1000)/1000,
					Math.round(res["SEMESTER_1_RESULT_3"]/res["SUMA_SEMESTER_1"]*1000)/1000,
					Math.round(res["SEMESTER_1_RESULT_4"]/res["SUMA_SEMESTER_1"]*1000)/1000,
					Math.round(res["SEMESTER_1_RESULT_5"]/res["SUMA_SEMESTER_1"]*1000)/1000
					]);

    renderHistogram($('#panel2'), [
					Math.round(res["SEMESTER_2_RESULT_1"]/res["SUMA_SEMESTER_2"]*1000)/1000, 
					Math.round(res["SEMESTER_2_RESULT_2"]/res["SUMA_SEMESTER_2"]*1000)/1000,
					Math.round(res["SEMESTER_2_RESULT_3"]/res["SUMA_SEMESTER_2"]*1000)/1000,
					Math.round(res["SEMESTER_2_RESULT_4"]/res["SUMA_SEMESTER_2"]*1000)/1000,
					Math.round(res["SEMESTER_2_RESULT_5"]/res["SUMA_SEMESTER_2"]*1000)/1000
					]);

    renderHistogram($('#panel3'), [
					Math.round(res["FINAL_GRADE_RESULT_1"]/res["SUMA_FINAL"]*1000)/1000, 
					Math.round(res["FINAL_GRADE_RESULT_2"]/res["SUMA_FINAL"]*1000)/1000,
					Math.round(res["FINAL_GRADE_RESULT_3"]/res["SUMA_FINAL"]*1000)/1000,
					Math.round(res["FINAL_GRADE_RESULT_4"]/res["SUMA_FINAL"]*1000)/1000,
					Math.round(res["FINAL_GRADE_RESULT_5"]/res["SUMA_FINAL"]*1000)/1000
					]);

    renderStats($('#panel1'), [
					Math.round(parseFloat(res["AVG_SEMESTER_1"])*1000)/1000,
					Math.round(parseFloat(res["VAR_SEMESTER_1"])*1000)/1000,
					Math.round(parseFloat(res["SEMESTAR_1_MEDIJANA"])*1000)/1000,
					Math.round(parseFloat(res["SEMESTAR_1_MODA"])*1000)/1000
					]);
	renderStats($('#panel2'), [
					Math.round(parseFloat(res["AVG_SEMESTER_2"])*1000)/1000,
					Math.round(parseFloat(res["VAR_SEMESTER_2"])*1000)/1000,
					Math.round(parseFloat(res["SEMESTAR_2_MEDIJANA"])*1000)/1000,
					Math.round(parseFloat(res["SEMESTAR_2_MODA"])*1000)/1000
					]);
	renderStats($('#panel3'), [
					Math.round(parseFloat(res["AVG_FINAL"])*1000)/1000,
					Math.round(parseFloat(res["VAR_FINAL"])*1000)/1000,
					Math.round(parseFloat(res["FINAL_GRADE_MEDIJANA"])*1000)/1000,
					Math.round(parseFloat(res["FINAL_GRADE_MODA"])*1000)/1000
					]);
  });

  $('#show-gradovi').on('click', function() {
    if (this.checked) {
      window.map.addLayer(window.mapLayerGradovi);
    } else {
      window.map.removeLayer(window.mapLayerGradovi);
    }
  });

  $('#show-ucilista').on('click', function() {
    if (this.checked) {
      window.map.addLayer(window.mapLayerUcilista);
    } else {
      window.map.removeLayer(window.mapLayerUcilista);
    }
  });

  $('#odberi-drzava').on('click', function() {
    ODV.Router.navigate({ grad: 'Сите' }, { trigger: true });
  });

  Backbone.history.start();
  
  _.defer(function() {
    if (Backbone.history.getFragment().length > 0) {
      return;
    }

    ODV.Router.navigate({
      godina: 'Учебна година 2012-2013',
      obrazovanie: 'основно',
      'studiska-godina': 'Сите',
      predmet: 'Сите',
      grad: 'Сите'
    }, { trigger: true });
  });
  
});
