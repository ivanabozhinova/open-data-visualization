window.ODV = window.ODV || {};

window.ODV.Predmeti = new Backbone.Collection();
window.ODV.Godini   = new Backbone.Collection();
window.ODV.StudiskiGodini = new Backbone.Collection();
window.ODV.Tipovi = new Backbone.Collection();
window.ODV.Rezultati = new Backbone.Model();

$.ajax({
  method: 'POST',
  url: '/query',
  data: {
    query: 'SELECT subject FROM OCENKI_PROSIRENO group by subject order by count(subject) desc, subject asc'
  },
  success: function(data, status, xhr) {
    console.log(data);

    window.ODV.Predmeti.reset(_.map(data['rows'], function(r) {
      return { name: r[0] };
    }));

    window.ODV.Predmeti.trigger('odv:loaded');
  },
  error: function(xhr, status, err) {
    window.ODV.Predmeti.trigger('odv:error', { xhr: xhr, status: status, err: err });
  }
});

$.ajax({
  method: 'POST',
  url: '/query',
  data: {
    query: 'SELECT DISTINCT school_year FROM OCENKI_PROSIRENO order by school_year'
  },
  success: function(data, status, xhr) {
    console.log(data);

    window.ODV.Godini.reset(_.map(data['rows'], function(r) {
      return { name: r[0] };
    }));

    window.ODV.Godini.trigger('odv:loaded');
  },
  error: function(xhr, status, err) {
    window.ODV.Godini.trigger('odv:error', { xhr: xhr, status: status, err: err });
  }
});

$.ajax({
  method: 'POST',
  url: '/query',
  data: {
    query: 'SELECT DISTINCT grade FROM OCENKI_PROSIRENO'
  },
  success: function(data, status, xhr) {
    console.log(data);

    window.ODV.StudiskiGodini.reset(_.map(data['rows'], function(r) {
      return { name: r[0] };
    }));

    window.ODV.StudiskiGodini.trigger('odv:loaded');
  },
  error: function(xhr, status, err) {
    window.ODV.StudiskiGodini.trigger('odv:error', { xhr: xhr, status: status, err: err });
  }
});

$.ajax({
  method: 'POST',
  url: '/query',
  data: {
    query: 'SELECT DISTINCT tip FROM OCENKI_PROSIRENO'
  },
  success: function(data, status, xhr) {
    console.log(data);

    window.ODV.Tipovi.reset(_.map(data['rows'], function(r) {
      return { name: r[0] };
    }));

    window.ODV.Tipovi.trigger('odv:loaded');
  },
  error: function(xhr, status, err) {
    window.ODV.Tipovi.trigger('odv:error', { xhr: xhr, status: status, err: err });
  }
});

$(function() {
  $.ajax({
    method: 'POST',
    url: '/query',
    data: {
      query: 'SELECT GRAD, LONGITUTDE, LATITUDE FROM GRADOVI_LOKACII'
    },
    success: function(data, status, xhr) {
      console.log(data);

      pointsGeojson = _.map(data['rows'], function(r) {
        return {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: [r[1] , r[2]]
          },
          properties: {
            title: r[0],
            description: 'Опис',
            'marker-color': '#FF6666'
          }
        };
      });

      window.mapLayerGradovi = L.mapbox.featureLayer().setGeoJSON(pointsGeojson).addTo(window.map);
      window.mapLayerGradovi.on('click', function(e){
        ODV.Router.navigate({ grad: e.layer.feature.properties.title }, { trigger: true });
      });
    },
    error: function(xhr, status, err) {
      console.log(err);
    }
  });

  $.ajax({
    method: 'POST',
    url: '/query',
    data: {
      query: 'select school, longitude, latitude, school_name from ucilista_lokacii'
    },
    success: function(data, status, xhr) {
      console.log(data);

      pointsGeojson = _.map(data['rows'], function(r) {
        return {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: [r[1] , r[2]]
          },
          properties: {
            title: r[0],
            description: r[3],
            'marker-color': '#6666FF',
            'marker-size': 'small'
          }
        };
      });

      window.mapLayerUcilista = L.mapbox.featureLayer().setGeoJSON(pointsGeojson).addTo(window.map);
      window.mapLayerUcilista.on('click', function(e) {
        ODV.Router.navigate({ uciliste: e.layer.feature.properties.title }, { trigger: true });
      });
    },
    error: function(xhr, status, err) {
      console.log(err);
    }
  });
});




