﻿var express = require('express');
var router = express.Router();
var oracledb = require('oracledb');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Визуелизација на Отворени Податоци' });
});

function izvrsiQveri(req, res, query, queryParametars, connectionParametars) {
	
	db = req.app.get('db');
	
	if (db == null) {
		res.status(500).send('Database not connected.');
		return;
	}
	
	db.getConnection(
	function(err, connection)
	{
		if (err) {
			console.error(err.message);
			res.status(500).send("Error conencting to database: " + err.message);
			return;
		}
		connection.execute(
		query,
		queryParametars || {},
		connectionParametars || {
			maxRows: 500,
			outFormat : oracledb.ARRAY
		},
		function(err, result)
		{
			if (err) {
				console.error(err.message);
				res.status(500).send("Error executing query: " + err.message);
			} else {
				result.request = {};
				result.request.body = req.body;
				result.request.query = req.query;
				res.json(result);
			}
			connection.release(
			function(err) {
				if (err) {
					console.error(err.message);
				}
			});
		});
	});
}

router.post('/query', function(req, res) {
  var query = req.body['query'];
  console.log("Service /query : " + query);
  izvrsiQveri(req, res, query);
});

router.get('/ocenki', ocenkiHandler);
router.post('/ocenki', ocenkiHandler);
function ocenkiHandler(req, res) {

	//res.send(req.body.sqlQuery);
	var params = req.body['params'];
	
	var tip = req.query['tip'] || req.body['tip'] || (params ? params['tip'] : null);
	var school_year = req.query['school_year'] || req.body['school_year'] || (params ? params['school_year'] : null);
	var school = req.query['school'] || req.body['school'] || (params ? params['school'] : null);
	var subject = req.query['subject'] || req.body['subject'] || (params ? params['subject'] : null);
	var grade = req.query['grade'] || req.body['grade'] || (params ? params['grade'] : null);
	var grad = req.query['grad'] || req.body['grad'] || (params ? params['grad'] : null);

	var sqlQuery = "select o3.*, decode(suma_semester_1, 0, null, 1, 0, (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+ SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+ SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) ) as var_semester_1, decode(suma_semester_2, 0, null, 1, 0, (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) ) as var_semester_2, decode(suma_final, 0, null, 1, 0, (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+ FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+ FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) ) as var_final from ( select o2.*, decode(suma_semester_1, 0, null, (SEMESTER_1_RESULT_1 * 1 + SEMESTER_1_RESULT_2 * 2 + SEMESTER_1_RESULT_3 * 3 + SEMESTER_1_RESULT_4 * 4 + SEMESTER_1_RESULT_5 * 5)/suma_semester_1) as avg_semester_1, decode(suma_semester_2, 0, null, (SEMESTER_2_RESULT_1 * 1 + SEMESTER_2_RESULT_2 * 2 + SEMESTER_2_RESULT_3 * 3 + SEMESTER_2_RESULT_4 * 4 + SEMESTER_2_RESULT_5 * 5)/suma_SEMESTER_2) as avg_semester_2, decode(suma_final, 0, null, (FINAL_GRADE_RESULT_1 * 1 + FINAL_GRADE_RESULT_2 * 2 + FINAL_GRADE_RESULT_3 * 3 + FINAL_GRADE_RESULT_4 * 4 + FINAL_GRADE_RESULT_5 * 5)/suma_final) as avg_final from ( select o1.*, (SEMESTER_1_RESULT_1 + SEMESTER_1_RESULT_2 + SEMESTER_1_RESULT_3+ SEMESTER_1_RESULT_4 + SEMESTER_1_RESULT_5 ) as suma_semester_1, (SEMESTER_2_RESULT_1 + SEMESTER_2_RESULT_2 + SEMESTER_2_RESULT_3+ SEMESTER_2_RESULT_4 + SEMESTER_2_RESULT_5) as suma_semester_2, (FINAL_GRADE_RESULT_1 + FINAL_GRADE_RESULT_2 + FINAL_GRADE_RESULT_3 + FINAL_GRADE_RESULT_4 + FINAL_GRADE_RESULT_5) as suma_final, ocenki_stat.medijana(SEMESTER_1_RESULT_1, SEMESTER_1_RESULT_2, SEMESTER_1_RESULT_3, SEMESTER_1_RESULT_4, SEMESTER_1_RESULT_5) as SEMESTAR_1_MEDIJANA, ocenki_stat.medijana(SEMESTER_2_RESULT_1, SEMESTER_2_RESULT_2, SEMESTER_2_RESULT_3, SEMESTER_2_RESULT_4, SEMESTER_2_RESULT_5) as SEMESTAR_2_MEDIJANA, ocenki_stat.medijana(FINAL_GRADE_RESULT_1, FINAL_GRADE_RESULT_2, FINAL_GRADE_RESULT_3, FINAL_GRADE_RESULT_4, FINAL_GRADE_RESULT_5) as FINAL_GRADE_MEDIJANA, ocenki_stat.moda(SEMESTER_1_RESULT_1, SEMESTER_1_RESULT_2, SEMESTER_1_RESULT_3, SEMESTER_1_RESULT_4, SEMESTER_1_RESULT_5) as SEMESTAR_1_MODA, ocenki_stat.moda(SEMESTER_2_RESULT_1, SEMESTER_2_RESULT_2, SEMESTER_2_RESULT_3, SEMESTER_2_RESULT_4, SEMESTER_2_RESULT_5) as SEMESTAR_2_MODA, ocenki_stat.moda(FINAL_GRADE_RESULT_1, FINAL_GRADE_RESULT_2, FINAL_GRADE_RESULT_3, FINAL_GRADE_RESULT_4, FINAL_GRADE_RESULT_5) as FINAL_GRADE_MODA from ( select sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1, sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2, sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3, sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4, sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5, sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2, sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3, sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5, sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1, sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2, sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3, sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4, sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5 from ocenki_prosireno ";
	var whereCond = [];
	var queryParam = {};

	if(tip != null){
		whereCond.push("tip = :tip");
		queryParam.tip = tip;
	}
	if(school_year != null){
		whereCond.push("school_year = :school_year");
		queryParam.school_year = school_year;
	}
	if(school != null){
		whereCond.push("school = :school");
		queryParam.school = school;
	}
	if(subject != null){
		whereCond.push("subject = :subject");
		queryParam.subject = subject;
	}
	if(grade != null){
		whereCond.push("grade = :grade");
		queryParam.grade = grade;
	}
	if(grad != null){
		whereCond.push("grad = :grad");
		queryParam.grad = grad;
	}

	if(whereCond.length > 0){
		sqlQuery+= " where " + whereCond.join(" AND ");
	}

	sqlQuery+=" ) o1 ) o2 ) o3";
	console.log("Service /ocenki: " + JSON.stringify(queryParam));
	izvrsiQveri(req, res, sqlQuery, queryParam);

}


module.exports = router;
