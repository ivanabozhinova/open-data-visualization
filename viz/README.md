Компалирање библиотеки
======================

Компалирањето опшано во точка 1 се прави само еднаш.
Во точка 2 е стартување на серверот.

1. Потребни алатки
==================

1.1. [node.js 64 битен](http://nodejs.org/)
-----------------------------------------


1.2. [Oracle instant client 11.2.0.1 64 битен (x86-64)](http://www.oracle.com/technetwork/topics/winx64soft-089540.html)
-----------------------------------------

Се симнува следниве две датотеки:

1.	Instant Client Package - **Basic**: All files required to run OCI, OCCI, and JDBC-OCI applications  
	Download instantclient-basic-win-x86-64-11.2.0.1.0.zip (55,084,309 bytes)
2.	Instant Client Package - **SDK**: Additional header files and an example makefile for developing Oracle applications with Instant Client  
	Download instantclient-sdk-win-x86-64-11.2.0.1.0.zip (1,412,561 bytes)

Двете датотеки се отпакуваат отпакуваат во c:\oracle\instantclient

За проверка, oci.dll треба да се наоѓа во c:\oracle\instantclient\oci.dll

1.3. [Python 2.7](https://www.python.org/)
-------------

Алтернативно може да користите [PYPY](http://pypy.org/) , сеедно е.  
Не е битна битноста тука.

Важно е Node.js и Instant Client да бидат со иста битност, и двете 32 или и двете 64, се препорачува 64.

Препорачливо е Python2.7 да се инсталира во C:\Python27\

1.4. Visual Studio 2010, 2012 или 2013
------------------------------------

1.5. Компајлирање библиотеки
----------------------------

Ако instant client и Python се ставени на препорачаните места, само се **клика двоен клик на Prepare.cmd**.

Ако не се на препорачаните, се отвора конзола, се оди во папката каде е скриптата и се повикува
```
prepare.cmd python_exe_path instant_client_folder
```

Пример:
```
prepare.cmd c:\python\python.exe c:\oracle\instantclient
```

Освен Оракл, погорната скрипта повикува да се инсталират сите останати потребни библиотеки.

Тука сме завршиле со компајлирање.

2. Извршување
=============

Во датотеката со лозинки config.json по потреба се внесуват соодветните податоци за конекција ако се сменети, ако не се оставаат тие што се.

Се подесува променлива на околината

```
set path=%path%;c:\oracle\instantclient
```

Потоа се извршува:

```
node bin\www
```

ИЛИ

```
npm start
```