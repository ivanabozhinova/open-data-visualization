if [%1]==[] goto error
if [%2]==[] goto error

if NOT "%VS100COMNTOOLS%"=="" set VISUAL_STUDIO=%VS100COMNTOOLS%
if NOT "%VS110COMNTOOLS%"=="" set VISUAL_STUDIO=%VS110COMNTOOLS%
if NOT "%VS120COMNTOOLS%"=="" set VISUAL_STUDIO=%VS120COMNTOOLS%
if "%VISUAL_STUDIO%"=="" goto vs_error

set PYTHON=%1
set ORACLE_IC=%2
set OCI_LIB_DIR=%ORACLE_IC%\sdk\lib\msvc
set OCI_INC_DIR=%ORACLE_IC%\sdk\include
call "%VISUAL_STUDIO%\..\..\VC\vcvarsall.bat" x64
set path=%PYTHON%;%ORACLE_IC%;%path%

call npm install oracledb
call npm install express
call npm install tunnel-ssh
call npm install body-parser
call npm install multer

:error
echo alatkata se koristi vaka:
echo %~n0 python2.7_exe_path instant_client_folder
goto exit

:vs_error
echo visual studio not installed

:exit