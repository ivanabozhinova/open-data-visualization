ВЕБ-сервиси
===========

1. Потребни алатки
==================

1.1. [node.js 64 битен](http://nodejs.org/)
-----------------------------------------


1.2. [Oracle instant client 11.2.0.1 64 битен (x86-64)](http://www.oracle.com/technetwork/topics/winx64soft-089540.html)
-----------------------------------------

Се симнува следниве две датотеки:

Instant Client Package - Basic: All files required to run OCI, OCCI, and JDBC-OCI applications  
Download instantclient-basic-win-x86-64-11.2.0.1.0.zip (55,084,309 bytes)

Instant Client Package - SDK: Additional header files and an example makefile for developing Oracle applications with Instant Client  
Download instantclient-sdk-win-x86-64-11.2.0.1.0.zip (1,412,561 bytes)

Двете датотеки се отпакуваат отпакуваат во c:\oracle\instantclient

1.3. [Python 2.7](https://www.python.org/)
-------------

Алтернативно може да користите [PYPY](http://pypy.org/) , сеедно е.  
Не е битна битноста тука.

Важно е Node.js и Instant Client да бидат со иста битност, и двете 32 или и двете 64, се препорачува 64.

1.4. Visual Studio 2010, 2012 или 2013
------------------------------------

1.5. Компајлирање библиотеки
----------------------------

Се отвора конзола, се оди до папката webservices и се повикува
```
prepare.cmd python_exe_path instant_client_folder
```

Пример:
```
prepare.cmd c:\python\python.exe c:\oracle\instantclient
```

Библиотеките што се користат се следиве (погорната скрипта ги инсталира и инв):
```
npm install oracledb
npm install express
npm install tunnel-ssh
npm install body-parser
npm install multer
```

2. Извршување
=============

Треба да се ископира датотеката со лозинки config.json во папката webservices.
Датитеката од безбедносни причини се наоѓа на GOOGLE DRIVE/tehnicki dokumenti и рачно се копира.

Се подесува променлива на околината

```
set path=%path%;c:\oracle\instantclient
```

Потоа се извршува: (не е потребно мануелно стартување на тунелот)

```
node app.js
```