process.env['NLS_LANG'] = '.UTF8'; //za oracle

var express = require('express');
var oracledb = require('oracledb');
var tunnel = require('tunnel-ssh');
var bodyParser = require('body-parser');
//var multer = require('multer'); 


var config = require('./config.json');

var app = express();
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(multer()); // for parsing multipart/form-data

var db = null;
var i = 1;


var sshServer = tunnel(
	{
		host: config.tunnel.host,
		username: config.tunnel.username,
		password: config.tunnel.password,
		dstPort:  config.tunnel.dstPort,
		localPort:  config.tunnel.localPort
	},
	function (error, result) {
		if (error) {
			console.error("tunnel error: " + error.message);
			return;
		}
		console.log('tunnel connected');
		oracledb.createPool(
		{
			user          : config.database.user,
			password      : config.database.password,
			connectString : config.database.connectString
		},
		function(error, pool) {
			if (error) {
				console.error(error.message);
				return;
			}
			db = pool;
		});
	}
);

app.get('/', function(request, response) {
	response.send("Welcome to Analiza na ocenki middle layer");
});

function izvrsiQveri(req, res, query, queryParametars, connectionParametars) {
	db.getConnection(
	function(err, connection)
	{
		if (err) {
			console.error(err.message);
			res.send("Error conencting to database: " + err.message);
			return;
		}
		connection.execute(
		query,
		queryParametars || {},
		connectionParametars || {
			maxRows: 500,
			outFormat : oracledb.ARRAY
		},
		function(err, result)
		{
			if (err) {
				console.error(err.message);
				res.send("Error executing query: " + err.message);
				return;
			} else {
				res.json(result);
			}
			connection.release(
			function(err) {
				if (err) {
					console.error(err.message);
				}
			});
		});
	});
}

function opsluziHistogram(req, res, grupiranje) {
	grupiranje = grupiranje || "DRZAVA";
	izvrsiQveri(req, res, "SELECT * "
		+ "FROM HISTOGRAM_PO_" + grupiranje);
}

app.get('/ocenki/hist/grad', function(req, res){
	opsluziHistogram(req, res, "GRAD");
});

app.get('/ocenki/hist/drzava', function(req, res){
	opsluziHistogram(req, res, "DRZAVA");
});

app.get('/ocenki/hist/predmet', function(req, res){
	opsluziHistogram(req, res, "PREDMET");
});

//EVIL, DO NOT FORGET, DO NOT WRITE AGAIN THIS CODE.
//WRIITEN BY DR.EVIL
//post-oto prima sodrzina application/x-www-form-urlencoded ili application/json
//kverito se naogja vo parametarot sqlQuery
//primer za urlencoded (body go sodrzi toa desno od prasalnikot) localhost:8000/query?sqlQuery=SELECT * FROM BLABLA
app.post('/query', function(req, res) {
	//res.send(req.body.sqlQuery);
	izvrsiQveri(req, res, req.body.sqlQuery);
});

app.get('/ocenki', ocenkiHandler);
app.post('/ocenki', ocenkiHandler);
function ocenkiHandler(req, res) {

	//res.send(req.body.sqlQuery);
	var params = req.body['params'];
	
	var tip = req.query['tip'] || req.body['tip'] || (params ? params['tip'] : null);
	var school_year = req.query['school_year'] || req.body['school_year'] || (params ? params['school_year'] : null);
	var school = req.query['school'] || req.body['school'] || (params ? params['school'] : null);
	var subject = req.query['subject'] || req.body['subject'] || (params ? params['subject'] : null);
	var grade = req.query['grade'] || req.body['grade'] || (params ? params['grade'] : null);
	var grad = req.query['grad'] || req.body['grad'] || (params ? params['grad'] : null);

	var sqlQuery = "select o3.*, decode(suma_semester_1, 0, null, 1, 0, (SEMESTER_1_RESULT_1*power(1-avg_semester_1, 2)+ SEMESTER_1_RESULT_2*power(2-avg_semester_1, 2)+ SEMESTER_1_RESULT_3*power(3-avg_semester_1, 2)+ SEMESTER_1_RESULT_4*power(4-avg_semester_1, 2)+ SEMESTER_1_RESULT_5*power(5-avg_semester_1, 2))/(suma_semester_1 -1) ) as var_semester_1, decode(suma_semester_2, 0, null, 1, 0, (SEMESTER_2_RESULT_1*power(1-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_2*power(2-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_3*power(3-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_4*power(4-avg_SEMESTER_2, 2)+ SEMESTER_2_RESULT_5*power(5-avg_SEMESTER_2, 2))/(suma_SEMESTER_2 -1) ) as var_semester_2, decode(suma_final, 0, null, 1, 0, (FINAL_GRADE_RESULT_1*power(1-avg_final, 2)+ FINAL_GRADE_RESULT_2*power(2-avg_final, 2)+ FINAL_GRADE_RESULT_3*power(3-avg_final, 2)+ FINAL_GRADE_RESULT_4*power(4-avg_final, 2)+ FINAL_GRADE_RESULT_5*power(5-avg_final, 2))/(suma_final -1) ) as var_final from ( select o2.*, decode(suma_semester_1, 0, null, (SEMESTER_1_RESULT_1 * 1 + SEMESTER_1_RESULT_2 * 2 + SEMESTER_1_RESULT_3 * 3 + SEMESTER_1_RESULT_4 * 4 + SEMESTER_1_RESULT_5 * 5)/suma_semester_1) as avg_semester_1, decode(suma_semester_2, 0, null, (SEMESTER_2_RESULT_1 * 1 + SEMESTER_2_RESULT_2 * 2 + SEMESTER_2_RESULT_3 * 3 + SEMESTER_2_RESULT_4 * 4 + SEMESTER_2_RESULT_5 * 5)/suma_SEMESTER_2) as avg_semester_2, decode(suma_final, 0, null, (FINAL_GRADE_RESULT_1 * 1 + FINAL_GRADE_RESULT_2 * 2 + FINAL_GRADE_RESULT_3 * 3 + FINAL_GRADE_RESULT_4 * 4 + FINAL_GRADE_RESULT_5 * 5)/suma_final) as avg_final from ( select o1.*, (SEMESTER_1_RESULT_1 + SEMESTER_1_RESULT_2 + SEMESTER_1_RESULT_3+ SEMESTER_1_RESULT_4 + SEMESTER_1_RESULT_5 ) as suma_semester_1, (SEMESTER_2_RESULT_1 + SEMESTER_2_RESULT_2 + SEMESTER_2_RESULT_3+ SEMESTER_2_RESULT_4 + SEMESTER_2_RESULT_5) as suma_semester_2, (FINAL_GRADE_RESULT_1 + FINAL_GRADE_RESULT_2 + FINAL_GRADE_RESULT_3 + FINAL_GRADE_RESULT_4 + FINAL_GRADE_RESULT_5) as suma_final, ocenki_stat.medijana(SEMESTER_1_RESULT_1, SEMESTER_1_RESULT_2, SEMESTER_1_RESULT_3, SEMESTER_1_RESULT_4, SEMESTER_1_RESULT_5) as SEMESTAR_1_MEDIJANA, ocenki_stat.medijana(SEMESTER_2_RESULT_1, SEMESTER_2_RESULT_2, SEMESTER_2_RESULT_3, SEMESTER_2_RESULT_4, SEMESTER_2_RESULT_5) as SEMESTAR_2_MEDIJANA, ocenki_stat.medijana(FINAL_GRADE_RESULT_1, FINAL_GRADE_RESULT_2, FINAL_GRADE_RESULT_3, FINAL_GRADE_RESULT_4, FINAL_GRADE_RESULT_5) as FINAL_GRADE_MEDIJANA, ocenki_stat.moda(SEMESTER_1_RESULT_1, SEMESTER_1_RESULT_2, SEMESTER_1_RESULT_3, SEMESTER_1_RESULT_4, SEMESTER_1_RESULT_5) as SEMESTAR_1_MODA, ocenki_stat.moda(SEMESTER_2_RESULT_1, SEMESTER_2_RESULT_2, SEMESTER_2_RESULT_3, SEMESTER_2_RESULT_4, SEMESTER_2_RESULT_5) as SEMESTAR_2_MODA, ocenki_stat.moda(FINAL_GRADE_RESULT_1, FINAL_GRADE_RESULT_2, FINAL_GRADE_RESULT_3, FINAL_GRADE_RESULT_4, FINAL_GRADE_RESULT_5) as FINAL_GRADE_MODA from ( select sum(SEMESTER_1_RESULT_1) as SEMESTER_1_RESULT_1, sum(SEMESTER_1_RESULT_2) as SEMESTER_1_RESULT_2, sum(SEMESTER_1_RESULT_3) as SEMESTER_1_RESULT_3, sum(SEMESTER_1_RESULT_4) as SEMESTER_1_RESULT_4, sum(SEMESTER_1_RESULT_5) as SEMESTER_1_RESULT_5, sum(SEMESTER_2_RESULT_1) as SEMESTER_2_RESULT_1, sum(SEMESTER_2_RESULT_2) as SEMESTER_2_RESULT_2, sum(SEMESTER_2_RESULT_3) as SEMESTER_2_RESULT_3, sum(SEMESTER_2_RESULT_4) as SEMESTER_2_RESULT_4, sum(SEMESTER_2_RESULT_5) as SEMESTER_2_RESULT_5, sum(FINAL_GRADE_RESULT_1) as FINAL_GRADE_RESULT_1, sum(FINAL_GRADE_RESULT_2) as FINAL_GRADE_RESULT_2, sum(FINAL_GRADE_RESULT_3) as FINAL_GRADE_RESULT_3, sum(FINAL_GRADE_RESULT_4) as FINAL_GRADE_RESULT_4, sum(FINAL_GRADE_RESULT_5) as FINAL_GRADE_RESULT_5 from ocenki_prosireno ";
	var whereCond = [];
	var queryParam = {};

	if(tip != null){
		whereCond.push("tip = :tip");
		queryParam.tip = tip;
	}
	if(school_year != null){
		whereCond.push("school_year = :school_year");
		queryParam.school_year = school_year;
	}
	if(school != null){
		whereCond.push("school = :school");
		queryParam.school = school;
	}
	if(subject != null){
		whereCond.push("subject = :subject");
		queryParam.subject = subject;
	}
	if(grade != null){
		whereCond.push("grade = :grade");
		queryParam.grade = grade;
	}
	if(grad != null){
		whereCond.push("grad = :grad");
		queryParam.grad = grad;
	}

	if(whereCond.length > 0){
		sqlQuery+= " where " + whereCond.join(" AND ");
	}

	sqlQuery+=" ) o1 ) o2 ) o3";
	izvrsiQveri(req, res, sqlQuery, queryParam);

}

app.listen(8000);